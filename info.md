# Présentation de la RTC

Cf. diapositives jointes à ce document.

Pour votre culture personnelle, quelques explications vous seront aussi données
concernant les horloges à crystal de type quartz ainsi que le protocole NTP.


# Mise en pratique

## Vérification de votre environnement

### Partie logiciel

Assurez-vous que votre ordinateur est correctement configuré pour travailler
avec les cartes Spider II (micro-controleur).

Les points suivants peuvent vous aider:

- si certaines méthodes ne sont pas reconnues, assurez-vous que l'assembly
(dll) correspondante soit activé (cf. "Projet" -> "Ajouter une référence" ->
"Extension" -> puis cocher par exemple "GHI.Hardware").

- si la communication ne s'établit pas entre visual studio (VS) et le
micro-controleur, allez dans les propriétés de votre projet "VS" ->
".Net Micro Framework" -> "Deployment" -> "Transport"
et choisissez "Serial", puis regénérez et lancer votre projet.
Vous obtiendrez alors une erreur.
Retournez dans le même menu, et sélectionner cette fois: "USB" et "G120-G120".
Cela forcera la prise en compte du mode USB.

### Partie matérielle

A l'aide d'un breakout board à 10 PIN connectable à un emplacement de type Z sur
le micro-controleur et d'une batterie à courant continu de 3.3V, mettez en place
une batterie RTC permettant de conserver l'heure lorsque le micro-controleur
n'est plus sous-tension.

## Prise en main de RealTimeClock et le temps Netmf

Sur la base des documents fournis, concevez dans l'ordre le code suivant pour:

- Afficher les valeurs de temps par défaut de la RTC et de netmf.
  Conservez ces valeurs dans un fichier texte à part. Elles vous serviront par
  la suite.
- Affecter une date et heure spécifique à la RTC, puis la récupérer depuis la
  RTC et l'assigner à netmf.
- Arrêter le programme, puis en créer un nouveau affichant seulement les valeurs
  de temps de la RTC et de netmf. Que constatez-vous?
- Créer un nouveau programme pour, au démarrage, fixer le temps netmf via le
  temps récupéré depuis la RTC.
- créez un programme détectant si la RTC a été débranchée de sa batterie et
  avertissez alors l'utilisateur via un message de debug.
- débranchez seulement l'alimentation du micro-controleur. Est-ce que le temps a
été perdu?
- débranchez aussi le port USB entre le micro-controleur et le PC puis
rebranchez le. Est-ce que le temps a été perdu?
- branchez une batterie 3V au microcontroleur et vérifiez que le temps est bien
conservé après avoir déconnecté toute alimentatioon.
  La batterie doit être conçue de la façon suivante:
  - alimentation 3V (type CR 1/1 AA par exemple).
  - utilisez un connecteur "breakout" GHI fournissant 10 connecteurs.
    branchez le + de la batterie au port P5 et le - au port GND.
    branchez le tout au connecteur Z du micro-controleur.
